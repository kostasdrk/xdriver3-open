#!/usr/bin/python

from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException

class XActionChains(ActionChains):
	_caller_prefix = "XActionChains"

	def __init__(self, driver):
		self._action_state = [None] # Need to contain a dummy element
		ActionChains.__init__(self, driver)

	def perform(self):
		if self._driver.w3c:
			pass
			# self.w3c_actions.perform()
		else:
			for action in self._actions:
				if action is ActionChains.move_to_element or action is ActionChains.move_to_element_with_offset: # We are about to change the focus to another element,
					self._action_state.pop(0) # So pop the last active element
				
				try: action()
				except (StaleElementReferenceException, NoSuchElementException) as ex:
					if not self._driver._StaleElementReference_handler(self._action_state[0]): # Recover the last active element
						raise 
					action() # Retry the last action only once


	''' The only methods that acceps a WebElement are the `move_to_element*` ones. All other ActionChains methods use this under the hood to locate the element.
		Thus, we only need to remember these methods to handle stale elements. '''
	def move_to_element(self, to_element):
		self._action_state.append(to_element)
		super(XActionChains, self).move_to_element(to_element)

	def move_to_element_with_offset(self, to_element, xoffset, yoffset):
		self._action_state.append(to_element)
		super(XActionChains, self).move_to_element_with_offset(to_element, xoffset, yoffset)