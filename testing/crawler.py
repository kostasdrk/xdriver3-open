#!/usr/bin/python

import sys
sys.path.insert(0, "..")

from xdriver.XDriver import XDriver
from xdriver.xutils.Regexes import Regexes

if __name__ == "__main__":
	xd = XDriver.boot(chrome = True)

	login_urls = []
	signup_urls = []

	target = "amazon.com"
	''' Configure crawl
	'''
	xd.crawl_init(target, bfs = True, depth = 2, follow = [Regexes.AUTH])

	# Crawl loop
	while xd.crawl_next():
		login_forms, signup_forms = xd.get_account_forms()
		
		if login_forms: login_urls.append(xd.current_url())
		if signup_forms: signup_urls.append(xd.current_url())

		if login_urls and signup_urls: break

	print("Login urls: %s" % login_urls)
	print("Signup urls: %s" % signup_urls)

	xd.quit()