#!/usr/bin/python

import json

from xdriver.XDriver import XDriver

if __name__ == "__main__":
	XDriver.enable_internal_proxy() # XDriver dedicated proxy, needed to spoof the headers
	xd = XDriver.boot(chrome = True)

	target = "http://instagram.com"
	spoofed_origin = "http://evil.com"
	xd.spoof_headers(domain_headers = [{"domain" : ".instagram.com", "headers" : {"Origin" : spoofed_origin}}])

	res = xd.get_redirection_flow(target)
	print(json.dumps(res, indent = 4)) # Check the request headers for the Origin header

	s = input("Exit..")
	xd.quit()